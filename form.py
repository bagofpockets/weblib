# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(912, 161)
        self.mainCentralwidget = QWidget(MainWindow)
        self.mainCentralwidget.setObjectName(u"mainCentralwidget")
        self.gridLayout = QGridLayout(self.mainCentralwidget)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setSizeConstraint(QLayout.SetMaximumSize)
        self.gridLayout.setContentsMargins(6, 3, 6, 0)
        self.browseButton = QPushButton(self.mainCentralwidget)
        self.browseButton.setObjectName(u"browseButton")

        self.gridLayout.addWidget(self.browseButton, 0, 1, 1, 1)

        self.novelURL = QLineEdit(self.mainCentralwidget)
        self.novelURL.setObjectName(u"novelURL")

        self.gridLayout.addWidget(self.novelURL, 2, 0, 1, 1)

        self.novelRemoveButton = QPushButton(self.mainCentralwidget)
        self.novelRemoveButton.setObjectName(u"novelRemoveButton")

        self.gridLayout.addWidget(self.novelRemoveButton, 4, 0, 1, 1)

        self.novelAddButton = QPushButton(self.mainCentralwidget)
        self.novelAddButton.setObjectName(u"novelAddButton")

        self.gridLayout.addWidget(self.novelAddButton, 3, 0, 1, 1)

        self.libPath = QLineEdit(self.mainCentralwidget)
        self.libPath.setObjectName(u"libPath")

        self.gridLayout.addWidget(self.libPath, 0, 0, 1, 1)

        self.novelsList = QListWidget(self.mainCentralwidget)
        self.novelsList.setObjectName(u"novelsList")
        self.novelsList.setEditTriggers(QAbstractItemView.SelectedClicked)
        self.novelsList.setViewMode(QListView.ListMode)
        self.novelsList.setSortingEnabled(True)

        self.gridLayout.addWidget(self.novelsList, 2, 1, 3, 1)

        self.updateLabel = QLabel(self.mainCentralwidget)
        self.updateLabel.setObjectName(u"updateLabel")
        self.updateLabel.setAlignment(Qt.AlignHCenter|Qt.AlignTop)

        self.gridLayout.addWidget(self.updateLabel, 7, 0, 1, 1)

        self.novelRefreshButton = QPushButton(self.mainCentralwidget)
        self.novelRefreshButton.setObjectName(u"novelRefreshButton")

        self.gridLayout.addWidget(self.novelRefreshButton, 7, 1, 1, 1)

        MainWindow.setCentralWidget(self.mainCentralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 912, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Weblib", None))
        self.browseButton.setText(QCoreApplication.translate("MainWindow", u"\u0412\u044b\u0431\u0440\u0430\u0442\u044c \u043f\u0430\u043f\u043a\u0443 \u0434\u043b\u044f \u0431\u0438\u0431\u043b\u0438\u043e\u0442\u0435\u043a\u0438", None))
        self.novelURL.setPlaceholderText(QCoreApplication.translate("MainWindow", u"\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u043a\u043d\u0438\u0433\u0443, \u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440 https://www.royalroad.com/fiction/21220/mother-of-learning", None))
        self.novelRemoveButton.setText(QCoreApplication.translate("MainWindow", u"\u0423\u0434\u0430\u043b\u0438\u0442\u044c", None))
        self.novelAddButton.setText(QCoreApplication.translate("MainWindow", u"\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c", None))
        self.libPath.setPlaceholderText(QCoreApplication.translate("MainWindow", u"\u041f\u0443\u0442\u044c \u043a \u043f\u0430\u043f\u043a\u0435 \u0431\u0438\u0431\u043b\u0438\u043e\u0442\u0435\u043a\u0438", None))
        self.updateLabel.setText(QCoreApplication.translate("MainWindow", u"\u041d\u0435 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e", None))
        self.novelRefreshButton.setText(QCoreApplication.translate("MainWindow", u"\u041e\u0431\u043d\u043e\u0432\u0438\u0442\u044c", None))
    # retranslateUi
