import os
import json
import configparser
from io import BytesIO
from typing import Tuple
from zipfile import ZipFile
from datetime import datetime
from urllib.parse import urlparse
from PySide2.QtWidgets import QMainWindow, QFileDialog, QMessageBox
from form import Ui_MainWindow
import requests
from apscheduler.schedulers.qt import QtScheduler

class App(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        
        self.browseButton.clicked.connect(self.browse_dirs)
        self.novelAddButton.clicked.connect(self.check_source)
        self.novelRemoveButton.clicked.connect(self.remove_novel)
        self.novelRefreshButton.clicked.connect(self.update)

        self.scheduler = QtScheduler()
        self.manifest = None
        self.manifest_path = None
        self.config = configparser.ConfigParser()
        if os.path.exists('weblib.ini'):
            self.config.read('weblib.ini')
            self.libPath.setText(self.config['Lib']['path'])
            self.manifest_path = os.path.normpath(os.path.join(self.config['Lib']['path'], 'manifest.weblib'))
            if os.path.exists(self.manifest_path):
                with open(self.manifest_path, 'r') as manifest_file:
                    self.manifest = json.load(manifest_file)
                self.__novellist_update()
        else:
            self.novelAddButton.setEnabled(False)
            self.novelRemoveButton.setEnabled(False)
            self.novelRefreshButton.setEnabled(False)
            self.novelURL.setEnabled(False)
        
        self.scheduler.start()

        self.supported_sources = {
            'www.royalroad.com': self.__royalroad}
    
    def closeEvent(self, event):
        self.scheduler.shutdown()

    def browse_dirs(self):
        options = QFileDialog.Options()
        selected_directory = QFileDialog.getExistingDirectory(self,"Выбрать папку библиотеки", options=options)
        if selected_directory:
            self.libPath.setText(selected_directory)
            if not os.path.exists('weblib.ini'):
                self.config['Lib'] = {'path': selected_directory}
            else:
                self.config['Lib']['path'] = selected_directory
            
            with open('weblib.ini', 'w') as configfile:
                self.config.write(configfile)
            
            self.novelAddButton.setEnabled(True)
            self.novelRemoveButton.setEnabled(True)
            self.novelRefreshButton.setEnabled(True)
            self.novelURL.setEnabled(True)

    def __royalroad(self, path: str, get: bool=False) -> Tuple[str, str]:
        source = path[1:].split('/', maxsplit=2)
        if source and len(source) > 1:
            id_source = source[1]
            if get:
                return ('royalroad', id_source)
            
            if not self.manifest_path:
                self.manifest_path = os.path.normpath(os.path.join(self.config['Lib']['path'], 'manifest.weblib'))
            
            if not self.manifest:
                self.manifest = {
                    'royalroad': [id_source]
                }
                
            elif id_source not in self.manifest['royalroad']:
               self.manifest['royalroad'].append(id_source) 
            
            with open(self.manifest_path, 'w') as manifest_file:
                json.dump(self.manifest, manifest_file)
            
            self.__novellist_update()

    def __novellist_update(self):
        if self.manifest:
            pretty_keys = {
                'royalroad': 'https://www.royalroad.com/fiction/'
            }
            
            self.novelsList.clear()
            for key, values in self.manifest.items():
                pretty_key = key
                if key in pretty_keys:
                    pretty_key = pretty_keys[key]
                
                for value in values:
                    self.novelsList.addItem(f'{pretty_key}{value}')

        if not self.scheduler.get_job('update'):
            self.scheduler.add_job(self.update, 'interval', minutes=1, max_instances=1, id='update') #minutes=1

    def check_source(self):
        novel_url = self.novelURL.text()
        if novel_url:
            try:
                url = urlparse(novel_url)
                if url.netloc in self.supported_sources:
                    self.supported_sources[url.netloc](url.path)
                    self.novelURL.setText('')
                else:
                    msg = QMessageBox()
                    msg.setWindowTitle('Ошибка')
                    msg.setText(f'Источник {url.netloc} не поддерживается.')
                    msg.exec_()
            except:
                msg = QMessageBox()
                msg.setWindowTitle('Ошибка')
                msg.setText(f'Источник {novel_url} не поддерживается.')
                msg.exec_()

    def remove_novel(self):
        if self.novelsList.currentItem():
            url = urlparse(self.novelsList.currentItem().text())
            source, id_source = self.supported_sources[url.netloc](url.path, get=True)
            
            self.manifest[source].remove(id_source)
            with open(self.manifest_path, 'w') as manifest_file:
                json.dump(self.manifest, manifest_file)
            
            self.__novellist_update()

    def update(self):
        try:
            response = requests.get('http://localhost:9000/api/library/update', json=json.dumps(self.manifest), stream=True)
        except:
            msg = QMessageBox()
            msg.setWindowTitle('Ошибка')
            msg.setText(f'Нет связи с сервером.')
            msg.exec_()
            return
        
        if response.status_code == 200:
            zip = ZipFile(BytesIO(response.content))
            zip.extractall(self.config['Lib']['path'])
            self.updateLabel.setText(f'Обновлено: {datetime.now()}')
